jQuery(document).ready(function($){
    $('.view-details-link').on('click', function(event){
        event.preventDefault();
    });

    $('.add-to-cart-link').on('click', function(event){
        event.preventDefault();

        var cart = $.cookie('cart');
        var good_id = $(this).data('id');
        var good = {};

        if (cart !== undefined) {
            console.log('test');
            cart = $.evalJSON(cart);
            var check = false;

            cart.forEach(function(item, i){
                console.log(cart);
                console.log(item);
                console.log(i);
                if (item !== null) {
                    if (item.id === good_id) {
                        ++item.mount;
                        check = true;
                    }
                } else {
                    if (i === 0) {

                    }
                    cart.splice(i, 1);
                }
                console.log(cart);

            });

            if (!check) {
                good.id = good_id;
                good.mount = 1;

                cart.push(good);
            }

            var count = $('.product-count').text();
            $('.product-count').text(++count);
        } else {
            good.id = good_id;
            good.mount = 1;

            cart = [good];
            var cartAmount = $('.cart_amount').html();
            $('.cart_amount').html(cartAmount + '<span class="product-count">1</span>');
        }

        $.cookie('cart', $.toJSON(cart));
    });

    $('.qty').on('change', function() {
        var parent = $(this).parents('.cart_item');
        var amount = $(this).val();
        var totalAmount = $(parent).find('.product-subtotal');
        var price = $(parent).find('.product-price').text().trim();

        $(totalAmount).text(amount * price);

        var cart = $.cookie('cart');
        var good_id = $(parent).data('id');
        var productCount = $('.product-count').text();

        cart = $.evalJSON(cart);
        cart.forEach(function(item, i){
            if (item.id === good_id) {
                if (parseInt(amount) === 0) {
                    cart.splice(i, 1);
                    $(this).parents('.cart_item').html('');
                } else if (amount > 0) {
                    productCount = productCount - item.mount;
                    item.mount = amount;
                    productCount = productCount + parseInt(amount);
                }
            }
        });
        if (cart.length === 0) {
            $.removeCookie('cart');
        }
        $.cookie('cart', $.toJSON(cart));
        $('.product-count').text(productCount);
    });

    $('.remove').on('click', function(event) {
        event.preventDefault();

        var parent = $(this).parents('.cart_item');
        var good_id = $(parent).data('id');
        var cart = $.cookie('cart');
        cart = $.evalJSON(cart);
        cart.forEach(function(item, i){
            if (item.id === good_id) {
                cart.splice(i, 1);
            }
        });
        if (cart.length === 0) {
            $.removeCookie('cart');
        } else {
            $.cookie('cart', $.toJSON(cart));
        }
        $(this).parents('.cart_item').html('');
    });

    $('.view-details-link').hover(
        function() {
            var thumb = $(this).parents('.single-product').find('.product-thumb');
            $(thumb).slideDown();
        },
        function() {
            var thumb = $(this).parents('.single-product').find('.product-thumb');
            $(thumb).slideUp();
        }
    );
});