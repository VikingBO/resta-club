<?php

use core\Renderer;

require_once dirname(__FILE__) . '/core/autoloader.php';

Renderer::header();

Renderer::indexPage();

Renderer::footer();

require_once PATH . 'templates/footer.php';

echo $html;