<?php

$page = explode('.', str_replace('/', '', $_SERVER['REQUEST_URI']))[0];
$payfile = '';
if ($page === 'payment') {
    $payfile = <<<PAYFILE
        <script src="/asserts/js/jquery.json.min.js"></script>
        <script src="/asserts/js/paymentForm.js"></script>
PAYFILE;
}

$html .= <<<"HTML"
        <!-- Latest jQuery form server -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        
        <!-- Bootstrap JS form CDN -->
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        
        <!-- Main Script -->
        <script src="/asserts/js/jquery.cookie.js"></script>
        <script src="/asserts/js/jquery.json.min.js"></script>
        <script src="/asserts/js/main.js"></script>

        ${payfile}
    </body>
</html>
HTML;
