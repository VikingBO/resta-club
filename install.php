<?php

use core\Renderer;

require_once(dirname(__FILE__) . '/core/autoloader.php');

Renderer::head();
Renderer::install();