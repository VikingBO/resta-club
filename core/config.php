<?php
$config = [
    'db' => [
        'user' => 'root',
        'pass' => '',
        'host' => '127.0.0.1',
        'name' => 'resta'
    ],
    'host' => 'http://' . $_SERVER['HTTP_HOST'] . '/',
    'realPath' => $_SERVER['DOCUMENT_ROOT'] . ((substr($_SERVER['DOCUMENT_ROOT'], -1) != '/') ? '/' : '')
];