<?php

function saveFiles()
{
    $result = '';

    if (!empty($_FILES['image_link']['tmp_name'])) {
        $check = false;

        switch ($_FILES['image_link']['error']) {
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                $result .= <<<"RESULT"
    <div class="error">
        <p>Размер отправляемого файла не может превышать 2 Mb</p>
    </div>
RESULT;

                break;
            case UPLOAD_ERR_OK:
                switch ($_FILES['image_link']['type']) {
                    case 'image/jpeg':
                    case 'image/png':
                    case 'image/gif':
                        $check = true;

                        break;
                    default:
                        $result .= <<<"RESULT"
    <div class="error">
        <p>Вы не можете загружать файлы данного формата.</p>
    </div>
RESULT;

                        break;
                }

                break;
            case UPLOAD_ERR_PARTIAL:
            case UPLOAD_ERR_NO_FILE:
            case UPLOAD_ERR_NO_TMP_DIR:
            case UPLOAD_ERR_CANT_WRITE:
            case UPLOAD_ERR_EXTENSION:
            default:
                $result .= <<<"RESULT"
    <div class="error">
        <p>При загрузке файла возникла ошибка, для решения вопроса обратитесь к администратору.</p>
    </div>
RESULT;

                break;
        }

        $fileName = uniqid().basename($_FILES['image_link']['name']);
        $uploadfile = PATH . 'asserts/img/' . $fileName;
        $linkfile = HOST . 'asserts/img/' . $fileName;

        if (!$check || !move_uploaded_file($_FILES['image_link']['tmp_name'], $uploadfile)) {
            $result .= <<<"RESULT"
    <div class="error">
        <p>Что то с файлом не так, он не прошел проверку и небыл загружен.</p>
    </div>
RESULT;

        }
    }

    return $result;
}

function foodAction()
{
    global $db;

    $result = '';

    if (!empty($_REQUEST)) {
        switch ($_REQUEST['action']) {
            case 'add':
                $linkfile = saveFiles();

                $insertFoodQuery = 'INSERT INTO `foods` (`title`, `price`, `image_link`, `description`, `category`) 
VALUES ("'.$_REQUEST['title'].'", "'.$_REQUEST['price'].'", "'.(!empty($linkfile) ? $linkfile : '').'", "'.(!empty($_REQUEST['description']) ? $_REQUEST['description'] : '').'","'.$_REQUEST['category'].'")';
                $insertFood = mysqli_query($db, $insertFoodQuery);
                if ($insertFood) {
                    $result .= <<<"RESULT"
<div class="success">
    <p>Блюдо добавлено.</p>
</div>
RESULT;
                }

                break;
            case 'delete':
                $deleteFoodQuery = mysqli_query($db, 'DELETE FROM `foods` WHERE `title` = "' . $_REQUEST['food_title'] . '"');
                if ($deleteFoodQuery) {
                    $result .= <<<"RESULT"
<div class="success">
    <p>Блюдо успешно удалено.</p>
</div>
RESULT;
                }

                break;
            default:
                break;
        }
    }

    return $result;
}

function userAction()
{
    global $db;

    $result = '';

    if (!empty($_REQUEST['action'])) {
        switch ($_REQUEST['action']) {
            case 'add':
                $rights = mysqli_real_escape_string($db, serialize([$_REQUEST['rights']]));
                $login = mysqli_real_escape_string($db, $_REQUEST['login']);
                $password = md5($_REQUEST['password']);
                $email = mysqli_real_escape_string($db, $_REQUEST['email']);
                $name = mysqli_real_escape_string($db, $_REQUEST['name']);

                $userInsertQuery = mysqli_query($db, 'INSERT INTO `users` (`login`,`name`,`password`,`email`,`rights`) 
VALUES ("'.$login.'", "'.$name.'", "'.$password.'" ,"'.$email.'" ,"'.$rights.'")');
                if ($userInsertQuery) {
                    $result .= <<<"RESULT"
<div class="success">
<p>Пользователь добавлен.</p>
</div>
RESULT;
                } else {
                    $result .= <<<"RESULT"
<div class="error">
    <p>По какой то причине пользователь не добавился</p>
</div>
RESULT;
                }

                break;
            case 'delete':
                $userDeleteRequest = mysqli_query($db, 'DELETE FROM `users` WHERE `login` = "'. $_REQUEST['login'].'"');
                if ($userDeleteRequest) {
                    $result .= <<<"RESULT"
<div class="success">
<p>Пользователь удален.</p>
</div>
RESULT;

                }

                break;
            default:
                break;
        }
    }

    return $result;
}

function getFoods()
{
    global $db;

    $foodsList = '';
    $foodsListQuery = 'SELECT `title`,`price`,`image_link`,`description`, `category`
        FROM `foods`';

    if ($_SESSION['rights'][0] !== 'grants') {
        $foodsListQuery .= 'WHERE `category` = "' . $_SESSION['rights'][0] . '"';
    }

    $foodsListQuery .= ' ORDER BY `id` DESC';
    $foodsListQuery = mysqli_query($db, $foodsListQuery);

    if (mysqli_num_rows($foodsListQuery) > 0) {
        while ($row = mysqli_fetch_assoc($foodsListQuery)) {
            if (@empty(file_get_contents($row['image_link'], false, null, 0, 10))) {
                $row['image_link'] = '/admin/asserts/img/no_img.png';
            }

            $foodsList .= <<<"LISTING"
<li class="list-item good">
    <span>${row['title']}</span>
    <span>${row['price']}</span>
    <span>${row['description']}</span> 
    <span><img src="${row['image_link']}" alt="${row['name']}"></span>
    <span>${row['category']}</span>   
    <span><a href="/admin/?action=delete&food_title=${row['title']}" >Удалить</a></span> 
</li>
LISTING;
        }
    }

    return $foodsList;
}

function getNotAdminUsers()
{
    global $db;

    $result = '';

    $usersQuery = mysqli_query($db, 'SELECT `login`,`rights` FROM `users` WHERE `login` != "admin"');
    if (mysqli_num_rows($usersQuery) > 0) {
        while ($row = mysqli_fetch_assoc($usersQuery)) {
            $rights = unserialize($row['rights'])[0];

            $result .= <<<"LISTING"
<li class="list-item user">
    <span>${row['login']}</span>
    <span>${rights}</span>
    <span><a href="/admin/users.php?action=delete&login=${row['login']}">Удалить</a></span>
</li>
LISTING;

        }
    }

    return $result;
}

function checkAuthorization()
{
    global $db;

    if (empty($_SESSION['login'])) {
        header('Location: ' . HOST . 'admin/login.php');
    }

    if (!empty($_SESSION['rights']) && $_SESSION['rights'][0] !== 'grants') {
        header('Location: ' . HOST . 'admin/');
    }

    $error = '';
    if (!empty($_POST['action']) && $_POST['action'] == 'login') {
        $user = mysqli_query($db, 'SELECT * FROM `users` WHERE `login` = "'.$_POST['login'].'"');
        if (mysqli_errno($db)) {
            echo mysqli_error($db);
        }

        if (mysqli_num_rows($user) > 0) {
            while ($row = mysqli_fetch_assoc($user)) {
                $_SESSION['login'] = $row['login'];
                $_SESSION['name'] = $row['name'];
                $_SESSION['email'] = $row['email'];
                $_SESSION['rights'] = unserialize($row['rights']);
                if (md5($_POST['password']) !== $row['password']) {
                    $error .= <<<"ERROR"
    <div class="error">
        <p>Пароль введен неверно. Проверьте раскладку и клавишу CAPS LOCK</p>
    </div>
ERROR;

                    $notAuth = 1;
                }
            }

            if(empty($notAuth)) {
                header('Location: ' . HOST . 'admin/');
            }
        } else {
            $error .= <<<"ERROR"
    <div class="error">
        <p>Указаный вами логин не найдет, убедитесь что вводите его верно.</p>
    </div>
ERROR;
        }
    }

    return $error;
}