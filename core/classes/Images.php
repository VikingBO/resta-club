<?php


namespace core;


class Images
{

    const TABLE = '`images`';

    public static function save()
    {
        global $config;

        if (!empty($_FILES['image_link']['tmp_name'])) {
            $check = false;

            switch ($_FILES['image_link']['error']) {
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new \Exception('Размер отправляемого файла не может превышать 2 Mb');

                    break;
                case UPLOAD_ERR_OK:
                    switch ($_FILES['image_link']['type']) {
                        case 'image/jpeg':
                        case 'image/png':
                        case 'image/gif':
                            $check = true;

                            break;
                        default:
                            throw new \Exception('Вы не можете загружать файлы данного формата.');

                            break;
                    }

                    break;
                case UPLOAD_ERR_PARTIAL:
                case UPLOAD_ERR_NO_FILE:
                case UPLOAD_ERR_NO_TMP_DIR:
                case UPLOAD_ERR_CANT_WRITE:
                case UPLOAD_ERR_EXTENSION:
                default:
                    throw new \Exception('При загрузке файла возникла ошибка, для решения вопроса обратитесь к администратору.');

                    break;
            }

            $fileName = uniqid().basename($_FILES['image_link']['name']);
            $uploadfile = $config['realPath'] . 'asserts/img/' . $fileName;
            $linkfile = $config['host'] . 'asserts/img/' . $fileName;

            if (!$check || !move_uploaded_file($_FILES['image_link']['tmp_name'], $uploadfile)) {
                throw new \Exception('Что то с файлом не так, он не прошел проверку и небыл загружен.');
            }
        }

        return $linkfile;
    }
}