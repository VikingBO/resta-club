<?php

namespace core;

use Exception;

class DataBase
{
    private $host;
    private $name;
    private $user;
    private $pass;

    public $db;

    public function __construct()
    {
        global $config;

        $dbConfig = $config['db'];
        $this->host = $dbConfig['host'];
        $this->name = $dbConfig['name'];
        $this->user = $dbConfig['user'];
        $this->pass = $dbConfig['pass'];
    }

    public function init()
    {
        $this->db = mysqli_connect(
            $this->host,
            $this->user,
            $this->pass,
            $this->name
        );

        if (empty($this->db)) {
            echo "Ошибка: Невозможно установить соединение с MySQL." . PHP_EOL;
            echo "Код ошибки errno: " . mysqli_connect_errno() . PHP_EOL;
            echo "Текст ошибки error: " . mysqli_connect_error() . PHP_EOL;
            exit;
        }

        return $this->db;
    }

    public function selectOne($sql)
    {
        $query = mysqli_query($this->db, $sql);

        $this->checkQueryResult($query);
        $result = mysqli_fetch_assoc($query);

        return $result[0];
    }

    public function selectAll($sql)
    {
        $query = mysqli_query($this->db, $sql);

        $this->checkQueryResult($query);
        $result = mysqli_fetch_assoc($query);

        return $result;
    }

    public function insertOne($table, array $params, array $values)
    {
        $queryParams = implode(',',$params);
        $queryValues = implode(',',$values);

        $queryString = 'INSERT INTO '.$table.'('.$queryParams.') VALUES ('.$queryValues.')';
        $query = mysqli_query($this->db, $queryString);
        $this->checkQueryResult($query);
    }

    public function insertAll($table ,array $params, array $values)
    {
        $queryParams = implode(',',$params);
        $queryValues = '';

        $i = 0;
        foreach ($values as $value) {
            ++$i;
            if ($i != 1) {
                $queryValues .= ',';
            }
            $queryValues .= '(' .implode(',',$value).')';
        }

        $queryString = 'INSERT INTO '.$table.'('.$queryParams.') VALUES '.$queryValues;
        $query = mysqli_query($this->db, $queryString);
        $this->checkQueryResult($query);
    }

    public function updateOne()
    {
        
    }

    public function updateAll()
    {
        
    }

    public function deleteOne()
    {
        
    }

    public function deletaAll()
    {
        
    }

    /**
     * @param $query
     * @throws Exception
     */
    private function checkQueryResult($query)
    {
        if (empty($query)) {
            $error = "Error text:" . mysqli_error($this->db).".\n\n".
                "Error number: " . mysqli_errno($this->db);
            throw new Exception($error);
        }
    }

    public function install()
    {
        // создаем необходимые таблицы если их нет
        $querys['users'] = 'CREATE TABLE IF NOT EXISTS '.Users::TABLE.' (
                              `id` INT(11) NOT NULL AUTO_INCREMENT,
                              `name` VARCHAR(255) NULL,
                              `login` VARCHAR(255) NOT NULL UNIQUE,
                              `password` VARCHAR(255) NOT NULL,
                              `email` VARCHAR(255) NOT NULL,
                              `address` VARCHAR(255) NULL,
                              `right_id` INT(11) NOT NULL,
                              PRIMARY KEY (`id`)
                            )
                            DEFAULT CHARACTER SET = utf8
                            DEFAULT COLLATE = utf8_general_ci';

        $querys['rights'] = 'CREATE TABLE IF NOT EXISTS '.Rights::TABLE.' (
                                `id` INT(11) NOT NULL AUTO_INCREMENT,
                                `name` VARCHAR(255) NOT NULL,
                                `pages` VARCHAR(255) NOT NULL,
                                PRIMARY KEY (`id`)
                            )
                            DEFAULT CHARACTER SET utf8
                            DEFAULT COLLATE utf8_general_ci';

        $querys['foods'] = 'CREATE TABLE IF NOT EXISTS '.Food::TABLE.' (
                              `id` INT(11) NOT NULL AUTO_INCREMENT,
                              `title` VARCHAR(255) NOT NULL UNIQUE,
                              `price` VARCHAR(255) NOT NULL,
                              `image_id` VARCHAR(255) NOT NULL,
                              `description` VARCHAR(255) NOT NULL,
                              `category_id` VARCHAR(255) NOT NULL,
                              PRIMARY KEY (`id`)
                            )
                            DEFAULT CHARACTER SET = utf8
                            DEFAULT COLLATE = utf8_general_ci';

        $querys['food_categorys'] = 'CREATE TABLE IF NOT EXISTS '.FoodCategory::TABLE.' (
                                      `id` INT(11) NOT NULL AUTO_INCREMENT,
                                      `path_alias` VARCHAR(255) NOT NULL UNIQUE,
                                      `name` VARCHAR(255) NOT NULL,
                                      `image_id` VARCHAR(255) NULL,
                                      PRIMARY KEY (`id`)
                                    )
                                    DEFAULT CHARACTER SET = utf8
                                    DEFAULT COLLATE = utf8_general_ci';

        $querys['cart'] = 'CREATE TABLE IF NOT EXISTS '.Cart::TABLE.' (
                              `id` INT(11) NOT NULL AUTO_INCREMENT,
                              `user_id` INT(11) NOT NULL,
                              `price` VARCHAR(255) NULL,
                              `info` VARCHAR(255) NULL,
                              `fields_version` INT(11) NOT NULL,
                              PRIMARY KEY (`id`)
                            )
                            DEFAULT CHARACTER SET = utf8
                            DEFAULT COLLATE = utf8_general_ci';

        $querys['payment'] = 'CREATE TABLE IF NOT EXISTS '.Payment::TABLE.' (
                                    `id` INT(11) NOT NULL AUTO_INCREMENT,
                                    `name` VARCHAR(255) NOT NULL,
                                    `type` VARCHAR(255) NOT NULL,
                                    `hash` VARCHAR(255) NOT NULL UNIQUE,
                                    `check` VARCHAR(255) NOT NULL,
                                    `order_id` INT(2) NOT NULL,
                                    PRIMARY KEY (`id`)
                                )
                                DEFAULT CHARACTER SET utf8
                                DEFAULT COLLATE utf8_general_ci';

        $querys['delivery'] = 'CREATE TABLE IF NOT EXISTS '.Delivery::TABLE.' (
                                    `id` INT(11) NOT NULL AUTO_INCREMENT,
                                    `name` VARCHAR(255) NOT NULL,
                                    `type` VARCHAR(255) NOT NULL UNIQUE,
                                    PRIMARY KEY (`id`)
                                )
                                DEFAULT CHARACTER SET utf8
                                DEFAULT COLLATE utf8_general_ci';

        $querys['courieres'] = 'CREATE TABLE IF NOT EXISTS '.Couriere::TABLE.' (
                                    `id` INT(11) NOT NULL AUTO_INCREMENT,
                                    `user_id` INT(11) NOT NULL,
                                    `status` VARCHAR(20) NOT NULL,
                                    PRIMARY KEY (`id`)
                                )
                                DEFAULT CHARACTER SET utf8
                                DEFAULT COLLATE utf8_general_ci';

        $querys['order'] = 'CREATE TABLE IF NOT EXISTS '.Order::TABLE.' (
                                `id` INT(11) NOT NULL AUTO_INCREMENT,
                                `user_id` INT(2) NOT NULL,
                                `cart_id` INT(2) NOT NULL,
                                `courier_id` INT(2) NOT NULL,
                                PRIMARY KEY (`id`)
                            )
                            DEFAULT CHARACTER SET utf8
                            DEFAULT COLLATE utf8_general_ci';

        $querys['images'] = 'CREATE TABLE IF NOT EXISTS '.Images::TABLE.' (
                                `id` INT(11) NOT NULL AUTO_INCREMENT,
                                `title` VARCHAR(255) NOT NULL,
                                `link` VARCHAR(255) NOT NULL,
                                `real_path` VARCHAR(255) NOT NULL,
                                PRIMARY KEY (`id`)
                            )';

        foreach ($querys as $query) {
            $queryResult = mysqli_query($this->db, $query);
            $this->checkQueryResult($queryResult);
        }

        $rights = $this->selectAll('SELECT * FROM '.Rights::TABLE.' WHERE `name` IN ("admin","customer","courier")');

        if (empty($rights)) {
            $rightsTable = '`rights`';
            $rightsParams = [
                '`name`',
                '`pages`'
            ];
            $rightsValue = [
                [
                    '"admin"',
                    '"all"'
                ],
                [
                    '"customer"',
                    '"pages"'
                ],
                [
                    '"courier"',
                    '"adminOrder"'
                ]
            ];
            $this->insertAll($rightsTable, $rightsParams, $rightsValue);
        }

        $admin = $this->selectAll('SELECT * FROM '.Users::TABLE.' WHERE `right_id` = 1');

        if (empty($admin)) {
            $usersTable = '`users`';
            $usersParams = [
                '`name`',
                '`login`',
                '`password`',
                '`email`',
                '`right_id`'
            ];
            $usersValues = [
                '"admin"',
                '"'.$_REQUEST['adminLogin'].'"',
                '"'.$_REQUEST['adminPass'].'"',
                '"admin@localhost"',
                1
            ];

            $this->insertOne($usersTable, $usersParams, $usersValues);
        }
    }
}