<?php

namespace core;

class Auth
{

    /** Checking user authorization */
    public function check()
    {
        if (empty($_SESSION['login'])) {
            header('Location: ' . HOST . 'admin/login.php');
        }

        if (!empty($_SESSION['rights']) && $_SESSION['rights'][0] !== 'grants') {
            header('Location: ' . HOST . 'admin/');
        }
    }

    /** get and login user */
    public function login()
    {
        global $db;

        $error = '';
        if (!empty($_POST['action']) && $_POST['action'] == 'login') {
            $user = mysqli_query($db, 'SELECT * FROM `users` WHERE `login` = "'.$_POST['login'].'"');
            if (mysqli_errno($db)) {
                echo mysqli_error($db);
            }

            if (mysqli_num_rows($user) > 0) {
                while ($row = mysqli_fetch_assoc($user)) {
                    $_SESSION['login'] = $row['login'];
                    $_SESSION['name'] = $row['name'];
                    $_SESSION['email'] = $row['email'];
                    $_SESSION['rights'] = unserialize($row['rights']);
                    if ($this->checkPassword($_POST['password'], $row['password'])) {
                        $error .= <<<"ERROR"
    <div class="error">
        <p>Пароль введен неверно. Проверьте раскладку и клавишу CAPS LOCK</p>
    </div>
ERROR;

                        $notAuth = 1;
                    }
                }

                if(empty($notAuth)) {
                    header('Location: ' . HOST . 'admin/');
                }
            } else {
                $error .= <<<"ERROR"
    <div class="error">
        <p>Указаный вами логин не найдет, убедитесь что вводите его верно.</p>
    </div>
ERROR;
            }
        }
    }

    private function checkPassword($password, $dbPassword)
    {
        return md5($password) !== $dbPassword;
    }
}