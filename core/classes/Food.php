<?php

namespace core;

class Food
{
    const TABLE = '`foods`';

    public function __construct()
    {
        $db = new DataBase();
        $db->init();
        $this->db = $db;
    }

    public function addOne()
    {
        $linkfile = Images::save();

        $foodTable = Food::TABLE;
        $insertFood = 'INSERT INTO `foods` (`title`, `price`, `image_link`, `description`, `category`) 
VALUES ("'.$_REQUEST['title'].'", "'.$_REQUEST['price'].'", "'.(!empty($linkfile) ? $linkfile : '').'", "'.(!empty($_REQUEST['description']) ? $_REQUEST['description'] : '').'","'.$_REQUEST['category'].'")';
        $insertFood = mysqli_query($db, $insertFoodQuery);
        if ($insertFood) {
            $result .= <<<"RESULT"
<div class="success">
    <p>Блюдо добавлено.</p>
</div>
RESULT;
        }
    }

    public function getAll()
    {
        return $this->db->selectAll('SELECT * FROM '.self::TABLE);
    }

    public function getPrice($id)
    {
        return $this->db->selectOne('SELECT DISTINCT price FROM '.self::TABLE.' WHERE id = ' . $id);
    }

    public function getAllToCategory($categoryId)
    {
        return $this->db->selectAll('SELECT * FROM '.self::TABLE.' WHERE category_id = '.$categoryId);
    }
}