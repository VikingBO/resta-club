<?php

namespace core;

class Renderer
{
    public static function head($otherFiles = '')
    {
        $result = '<!DOCTYPE html>
            <html lang="ru">
                </head>
                    <meta charset="utf-8"/>
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                    <title>{$title}</title>
                    
                    <!-- Google Fonts -->
                    <link href="http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600" rel="stylesheet" type="text/css">
                    <link href="http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300" rel="stylesheet" type="text/css">
                    <link href="http://fonts.googleapis.com/css?family=Raleway:400,100" rel="stylesheet" type="text/css">
                    
                    <!-- Bootstrap -->
                    <link rel="stylesheet" href="/asserts/css/bootstrap.min.css">
                    
                    <!-- Font Awesome -->
                    <link rel="stylesheet" href="/asserts/css/font-awesome.min.css">
                    
                    <!-- Custom CSS -->
                    <link rel="stylesheet" href="/asserts/css/style.css">
                    
                    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
                    <!-- WARNING: Respond.js doesn`t work if you view the page via file:// -->
                    <!--[if lt IE 9]>
                      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
                      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
                    <![endif]-->
                    
                    '.$otherFiles.'
                </head>';

        echo $result;
    }

    public static function header()
    {
        $result = '';
        $title = (!empty($title)) ? $title : 'Магазин индусской кухни';

        $foodCategory = new FoodCategory();
        $foodCategoryList = $foodCategory->getAll();

        if (count($foodCategoryList) > 0) {
            $url = explode('.',substr($_SERVER['REQUEST_URI'], 1));

            $active = empty($_REQUEST['category']) && $url[0] === 'index' ? 'class="active"' : '';

            $categoryList = '<li '.$active.'>
                                <a href="index.php">Главная</a>
                            </li>';

            while ($row = $foodCategoryList) {
                $active = (!empty($_REQUEST['category']) && $_REQUEST['category'] == $row['title']) ? 'class="active"' : '';

                $categoryList .= '<li '.$active.'>
                                    <a href="index.php?category='.$row['title'].'">'.$row['title'].'</a>
                                </li>';
            }
        }

        $cartCount = '';
        $cartPrice = '';
        if (!empty($_COOKIE['cart'])) {
            $cart = json_decode($_COOKIE['cart'], true);
            $cartAmount = 0;
            $cartPrice = 0;
            foreach ($cart as $good) {
                $cartAmount += $good['mount'];

                $food = new Food();
                $goodPrice = $food->getPrice($good['id']);
                if (!empty($goodPrice)) {
                    $cartPrice += $goodPrice['price'] * $good['mount'];
                }
            }

            if (!empty($cartAmount)) {
                $cartPrice =
                $cartCount = '<span class="product-count">' . $cartAmount . '</span>';
            }
        }


        $page = explode('.', str_replace('/', '', $_SERVER['REQUEST_URI']))[0];
        $payfile = '';
        if ($page === 'payment') {
            $payfile = '<link rel="stylesheet" href="/asserts/css/paymentForm.css">';
        }

        $result .= '<body>
                    <div class="header-area">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="user-menu">
                                        <ul>
                                            <li><a href="/admin/"><i class="fa fa-user"></i> Login</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- End header area -->
                    <div class="site-branding-area">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="logo">
                                        <h1>
                                            <a href="/">
                                                <img alt="" src="/asserts/img/logo.png">
                                            </a>
                                        </h1>
                                    </div>
                                </div>
                    
                                <div class="col-sm-6">
                                    <div class="shopping-item">
                                        <a class="cart_amount" href="cart.php">Корзина<i class="fa fa-shopping-cart"></i>'.$cartCount.'</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            
                    <div class="mainmenu-area">
                        <div class="container">
                            <div class="row">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                        <span class="sr-only">Переключение меню</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div> 
                                <div class="navbar-collapse collapse">
                                    <ul class="nav navbar-nav">
                                        '.$categoryList.'
                                    </ul>
                                </div>  
                            </div>
                        </div>
                    </div>';

        echo $result;
    }

    public static function footer()
    {
        $result = '';

        $page = explode('.', str_replace('/', '', $_SERVER['REQUEST_URI']))[0];
        $payfile = '';
        if ($page === 'payment') {
            $payfile = '<script src="/asserts/js/jquery.json.min.js"></script>
                        <script src="/asserts/js/paymentForm.js"></script>';
        }

        $result .= '<!-- Latest jQuery form server -->
                    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
                    
                    <!-- Bootstrap JS form CDN -->
                    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
                    
                    <!-- Main Script -->
                    <script src="/asserts/js/jquery.cookie.js"></script>
                    <script src="/asserts/js/jquery.json.min.js"></script>
                    <script src="/asserts/js/main.js"></script>
            
                    '.$payfile.'
                </body>
            </html>';

        echo $result;
    }

    public static function indexPage()
    {
        $result = '';
        $items = '';
        $itemsQuery = 'SELECT * FROM `foods`';

        if (!empty($_REQUEST['category'])) {
            $category = $_REQUEST['category'];
            $itemsQuery .= ' WHERE `category` = "' . $_REQUEST['category'] . '"';
        } else {
            $category = 'Главная';
            $itemsQuery .= '';
        }

        $itemsQuery .= ' ORDER BY `id`';

        $itemsListQuery = mysqli_query($db, $itemsQuery);

        if (!empty($itemsListQuery) && mysqli_num_rows($itemsListQuery) > 0) {
            while ($row = mysqli_fetch_assoc($itemsListQuery)) {
                if (@empty(file_get_contents($row['image_link'], false, null, 0, 10))) {
                    $row['image_link'] = '/admin/asserts/img/no_img.png';
                }

                $items .= <<<"LISTING"
<div class="single-product col-md-3 col-sm-6">
    <div class="product-f-image">
        <img src="${row['image_link']}" alt="${row['title']}">
        <div class="product-hover">
            <a href="#" data-id="${row['id']}" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> В корзину</a>
            <a href="" class="view-details-link"><i class="fa fa-link"></i> Описание</a>
        </div>
    </div>
    
    <h2>${row['title']}</h2>
    
    <div class="product-thumb">${row['description']}</div>
    
    <div class="product-carousel-price">
        <ins>${row['price']}</ins>
    </div> 
</div>
LISTING;
            }
        }

        $result .= <<<"HTML"
<div class="product-big-title-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="product-bit-title text-center">
                    <h2>${category}</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="single-product-area">
    <div class="container">
        <div class="row">
            ${items}
        </div>
    </div>
</div>
HTML;

        echo $result;
    }

    public static function install()
    {
        if (!empty($_REQUEST['adminLogin']) && !empty($_REQUEST['adminPass'])) {
            $db = new DataBase();
            $db->init();
            $db->install();
        }

        $result = '<div class="container">
                    <div class="row">
                        <div class="col-lg-4 col-lg-offset-4">
                            <form action="" method="post">
                                <div class="form-group">
                                    <span id="helpBlock" class="help-block">
                                        Для установки сайта необходимо ввести логин и пароль для пользователя "Администратор"
                                    </span>
                                </div>
                                <div class="form-group">
                                    <label for="adminLogin">Логин администратора</label>
                                    <input type="text" name="adminLogin" class="form-control" id="adminLogin" placeholder="Логин">
                                </div>
                                <div class="form-group">
                                    <label for="adminPass">Пароль администратора</label>
                                    <input type="password" name="adminPass" class="form-control" id="adminPass" placeholder="Password">
                                </div>
                                <button type="submit" class="btn btn-default">Начать установку сайта</button>
                            </form>
                        </div>
                    </div>
                </div>';

        echo $result;
    }
}