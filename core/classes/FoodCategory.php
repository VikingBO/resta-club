<?php

namespace core;

class FoodCategory
{
    const TABLE = '`food_category`';

    private $db;

    public function __construct()
    {
        $this->db = new DataBase();
        $this->db->init();
    }

    public function getAll()
    {
        return $this->db->selectAll('SELECT * FROM ' . self::TABLE . ' ORDER BY `id` DESC');
    }
}