<?php
session_start();
require_once 'config.php';

$classesDir = dirname(__FILE__) . '/classes';

if (is_dir($classesDir)) {
    $classes = scandir($classesDir);

    if (!empty($classes) && count($classes) > 2) {
        foreach ($classes as $class) {
            if ($class != '.' && $class != '..') {
                require_once($classesDir . '/' .  $class);
            }
        }
    }

} else {
    echo 'Not find classes folder.';
    exit;
}