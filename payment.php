<?php
// задаем путь до папки сайта
define('PATH', $_SERVER['DOCUMENT_ROOT'] . ((substr($_SERVER['DOCUMENT_ROOT'], -1) != '/') ? '/' : ''));

if (!empty($_POST['cartInfo'])) {
    $paymentHash = md5(json_encode($_POST['cartInfo']) . date('dmY',time()));
    $cartInfo = json_encode($_POST['cartInfo']);

    $saveCartInfo = 'INSERT INTO cart_information (`payment_hash`,`payment_check`,`cart_price`,`cart_info`)
                    VALUE ('.$paymentHash.',"false",'.$cartPrice.','.$cartInfo.')';
}

require_once PATH . 'core/help.php';
require_once PATH . 'templates/header.php';

$html .= <<<PAYMENT
<div class="container-fluid">
    <div class="creditCardForm">
        <div class="heading">
            <h1>Подтверждение оплаты</h1>
        </div>
        <div class="payment">
            <form action="." >
                <div class="form-group owner">
                    <label for="owner">Владелец</label>
                    <input type="text" class="form-control" id="owner">
                </div>
                <div class="form-group CVV">
                    <label for="cvv">CVV</label>
                    <input type="text" class="form-control" id="cvv">
                </div>
                <div class="form-group" id="card-number-field">
                    <label for="cardNumber">Номер карты</label>
                    <input type="text" class="form-control" id="cardNumber">
                </div>
                <div class="form-group" id="expiration-date">
                    <label>Срок действия</label>
                    <select>
                        <option value="01">Январь</option>
                        <option value="02">Февраль</option>
                        <option value="03">Март</option>
                        <option value="04">Апрель</option>
                        <option value="05">Май</option>
                        <option value="06">Июнь</option>
                        <option value="07">Июль</option>
                        <option value="08">Август</option>
                        <option value="09">Сентябрь</option>
                        <option value="10">Октябрь</option>
                        <option value="11">Ноябрь</option>
                        <option value="12">Декабрь</option>
                    </select>
                    <select>
                        <option value="16">2016</option>
                        <option value="17">2017</option>
                        <option value="18">2018</option>
                        <option value="19">2019</option>
                        <option value="20">2020</option>
                        <option value="21">2021</option>
                    </select>
                </div>
                <div class="form-group" id="credit_cards">
                    <img src="/asserts/img/visa.jpg" id="visa" alt="visa">
                    <img src="/asserts/img/mastercard.jpg" id="mastercard" alt="mastercard">
                    <img src="/asserts/img/amex.jpg" id="amex" alt="amex">
                </div>
                <div class="form-group" id="pay-now">
                    <button type="submit" class="btn btn-default" id="confirm-purchase">Отправить</button>
                </div>
            </form>
        </div>
    </div>
</div>
PAYMENT;

require_once PATH . 'templates/footer.php';

echo $html;
