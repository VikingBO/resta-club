<?php

// задаем путь до папки сайта
define('PATH', $_SERVER['DOCUMENT_ROOT'] . ((substr($_SERVER['DOCUMENT_ROOT'], -1) != '/') ? '/' : ''));

require_once PATH . 'core/help.php';
require_once PATH . 'templates/header.php';

$cartItems = '';
$cartPrice = 0;
if (!empty($cart)) {
    foreach ($cart as $good) {
        $foodsQuery = 'SELECT * FROM `foods` WHERE id = ' . $good['id'] . ' ORDER BY `id`';
        $foodsListQuery = mysqli_query($db, $foodsQuery);

        if (!empty($foodsListQuery) && mysqli_num_rows($foodsListQuery) > 0) {
            while ($row = mysqli_fetch_assoc($foodsListQuery)) {
                if (@empty(file_get_contents($row['image_link'], false, null, 0, 10))) {
                    $row['image_link'] = '/admin/asserts/img/no_img.png';
                }

                $totalPrice = $row['price'] * $good['mount'];
                $cartPrice += $totalPrice;

                $cartItems .= <<<CART
<tr class="cart_item" data-id="${row['id']}">
    <td class="product-remove">
        <a title="Удалить товар из корзины" class="remove" href="#">×</a> 
    </td>

    <td class="product-thumbnail">
        <img width="145" height="145" alt="poster_1_up" class="shop_thumbnail" src="${row['image_link']}">
    </td>

    <td class="product-name">
        ${row['title']}
    </td>

    <td class="product-price">
        <span class="amount">${row['price']}</span> 
    </td>

    <td class="product-quantity">
        <div class="quantity buttons_added">
            <input type="number" size="4" class="input-text qty text" title="Qty" value="${good['mount']}" min="0" step="1">
        </div>
    </td>

    <td class="product-subtotal">
        <span class="amount">${totalPrice}</span> 
    </td>
</tr>
CART;
            }
        }
    }
}

$html .= <<<"HTML"
    <div class="single-product-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" action="/payment.php">
                        <input type="hidden" name="cartPrice" value="${cartPrice}">
                    
                        <table cellspacing="0" class="shop_table cart">
                            <thead>
                                <tr>
                                    <th class="product-remove">Удалить</th>
                                    <th class="product-thumbnail">Изображение</th>
                                    <th class="product-name">Товар</th>
                                    <th class="product-price">Цена</th>
                                    <th class="product-quantity">Количество</th>
                                    <th class="product-subtotal">Всего</th>
                                </tr>
                            </thead>
                            <tbody>
                                ${cartItems}
                                <tr>
                                    <td class="actions" colspan="6">
                                        <input type="submit" value="Оплатить" name="proceed" class="checkout-button button alt wc-forward">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
HTML;

require_once PATH . 'templates/footer.php';

echo $html;
