<?php

// задаем путь до папки сайта
define('PATH', $_SERVER['DOCUMENT_ROOT'] . ((substr($_SERVER['DOCUMENT_ROOT'], -1) != '/') ? '/' : ''));

require_once PATH .'core/help.php';

session_destroy();
header("Location: " . HOST . "admin/");