<?php
// задаем путь до папки сайта
define('PATH', $_SERVER['DOCUMENT_ROOT'] . ((substr($_SERVER['DOCUMENT_ROOT'], -1) != '/') ? '/' : ''));

require_once PATH .'core/help.php';
require_once PATH . 'admin/templates/header.php';

if (empty($_SESSION['login'])) {
    header('Location: ' . HOST . 'admin/login.php');
}

if ($_SESSION['rights'][0] !== 'grants') {
    header('Location: ' . HOST . 'admin/');
}

$result = '';
$categoryList = '';

require_once PATH . 'admin/templates/menu.php';

$result .= saveFiles();

if (!empty($_REQUEST)) {
    switch ($_REQUEST['action']) {
        case 'add':
            $insertCategoryQuery = 'INSERT INTO `food_categorys` (`title`, `name`, `image_link`, `description`) 
VALUES ("'.$_REQUEST['title'].'", "'.$_REQUEST['name'].'", "'.(!empty($linkfile) ? $linkfile : '').'", "'.(!empty($_REQUEST['description']) ? $_REQUEST['description'] : '').'")';
            $insertCategory = mysqli_query($db, $insertCategoryQuery);
            if ($insertCategory) {
                $result .= <<<"RESULT"
<div class="success">
    <p>Категория добавлена.</p>
</div>
RESULT;
            }

            break;
        case 'delete':
            $deleteCategoryQuery = mysqli_query($db, 'DELETE FROM `food_categorys` WHERE `title` = "' . $_REQUEST['category_title'] . '"');
            if ($deleteCategoryQuery) {
                $result .= <<<"RESULT"
<div class="success">
    <p>Категория успешно удалена.</p>
</div>
RESULT;
            }

            break;
        default:
            break;
    }
}

$categoryListQuery = mysqli_query($db, 'SELECT `title`,`name`,`image_link`,`description` FROM `food_categorys` ORDER BY `id` DESC');
if (mysqli_num_rows($categoryListQuery) > 0) {
    while ($row = mysqli_fetch_assoc($categoryListQuery)) {
        $categoryList .= <<<"LISTING"
<li class="list-item categorie">
    <span>${row['title']}</span>
    <span>${row['name']}</span>
    <span>${row['description']}</span> 
    <span><img src="${row['image_link']}" alt="${row['name']}"></span>  
    <span><a href="/admin/categories.php?action=delete&category_title=${row['title']}" >Удалить</a></span> 
</li>
LISTING;

    }
}

$html .= <<<"HTML"
<div class="admin_wrap">
    <div class="left_side">${menu}</div>
    <div class="right_side">
        ${result}
        <h1>Список категорий</h1>
        <ul class="category list">
            <li class="list-item">
                <form id="category" class="form" action="" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="MAX_FILE_SIZE" value="2000000" />
                    <input type="hidden" name="action" value="add">
                    <input type="text" name="title" placeholder="Алиас категории" required>
                    <input type="text" name="name" placeholder="Название категории" required>
                    <textarea rows="1" cols="20" name="description"></textarea>
                    <label for="image_link">Выберите картинку</label>
                    <input id="image_link" type="file" name="image_link" placeholder="Изображение категории">
                    <label for="submit_category_form">Добавить категорию</label>
                    <input id="submit_category_form" type="submit">
                </form>
            </li>
            ${categoryList}        
        </ul>    
    </div>
</div>
HTML;

require_once PATH . 'admin/templates/footer.php';

echo $html;