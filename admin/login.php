<?php
// задаем путь до папки сайта
define('PATH', $_SERVER['DOCUMENT_ROOT'] . ((substr($_SERVER['DOCUMENT_ROOT'], -1) != '/') ? '/' : ''));

require_once PATH . 'core/help.php';
require_once PATH . 'admin/templates/header.php';

$error = checkAuthorization();

    $html .= <<<"HTML"
${error}
<div class="wrap">
    <form id="authorization_form" action="" method="post" name="authorization">
        <input type="hidden" name="action" value="login">
        <input type="text" name="login" placeholder="Введите логин:">
        <br>           
        <input type="password" name="password" placeholder="Введите пароль:">
        <br>
        <input type="submit"  value="Отправить">
    </form>    
</div>
HTML;

require_once "templates/footer.php";

echo $html;