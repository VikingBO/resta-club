<?php
// задаем путь до папки сайта
define('PATH', $_SERVER['DOCUMENT_ROOT'] . ((substr($_SERVER['DOCUMENT_ROOT'], -1) != '/') ? '/' : ''));

require_once PATH .'core/help.php';
require_once PATH . 'admin/templates/header.php';

$result = '';
$usersList = '';

require_once PATH . 'admin/templates/menu.php';

$result .= userAction();
$users .= getNotAdminUsers();

$html .= <<<"HTML"
<div class="admin_wrap">
    <div class="left_side">${menu}</div>
    <div class="right_side">
        ${result}
        <h1>Список пользователей</h1>
        <ul class="users list">
            <li class="list-item">
                <form id="user" class="form" action="" method="post">
                    <input type="hidden" name="action" value="add">
                    <input type="text" name="login" placeholder="Логин пользователя" required>
                    <input type="password" name="password" placeholder="Пароль пользователя" required>
                    <input type="text" name="name" placeholder="Имя пользователя" required>
                    <input type="email" name="email" placeholder="Email пользователя" required>
                    <input type="text" name="rights" placeholder="Алиас категории пользователя" required>
                    <label for="submit_user_form">Добавить пользователя</label>
                    <input id="submit_user_form" type="submit">
                </form>
            </li>
            ${users}
        </ul>    
    </div>
</div>
HTML;

require_once PATH . 'admin/templates/footer.php';

echo $html;