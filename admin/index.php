<?php
// задаем путь до папки сайта
define('PATH', $_SERVER['DOCUMENT_ROOT'] . ((substr($_SERVER['DOCUMENT_ROOT'], -1) != '/') ? '/' : ''));

require_once PATH . 'core/help.php';
require_once PATH . 'admin/templates/header.php';

if (empty($_SESSION['login'])) {
    header('Location: ' . HOST . 'admin/login.php');
}

$result = '';
$foodsList = '';

require_once PATH . 'admin/templates/menu.php';

$result .= saveFiles();
$result .= foodAction();
$foodsList = getFoods();

$html .= <<<"HTML"
<div class="admin_wrap">
    <div class="left_side">${menu}</div>
    <div class="right_side">
        ${result}
        <h1>Список блюд</h1>
        <ul class="food list">
            <li class="list-item">
                <form id="food" class="form" action="" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="MAX_FILE_SIZE" value="2000000" />
                    <input type="hidden" name="action" value="add">
                    <input type="text" name="title" placeholder="Алиас блюда" required>
                    <input type="number" name="price" placeholder="Цена блюда" required>
                    <textarea rows="1" cols="20" name="description" placeholder="Описание блюда"></textarea>
                    <label for="image_link">Выберите картинку</label>
                    <input id="image_link" type="file" name="image_link" placeholder="Изображение блюда">
HTML;

if ($_SESSION['rights'][0] == 'grants') {
    $html .= <<<"HTML"
                    <input type="text" name="category" placeholder="Категория блюда" required>
HTML;
}

$html .= <<<"HTML"
                    <label for="submit_food_form">Добавить блюдо</label>
                    <input id="submit_food_form" type="submit">
                </form>
            </li>
            ${foodsList}        
        </ul>    
    </div>
</div>
HTML;

require_once PATH . 'admin/templates/footer.php';

echo $html;