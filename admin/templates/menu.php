<?php
$menu = '<ul class="admin_menu">';
if ($_SESSION['rights'][0] == 'grants') {
    $menu .= <<<"MENU"
<li class="admin_menu_item">
    <a href="/admin/users.php">Пользователи</a>
</li>
<li class="admin_menu_item">
    <a href="/admin/categories.php">Категории</a>
</li>
<li class="admin_menu_item">
    <a href="/admin/index.php">Блюда</a>
</li>
MENU;
}
$menu .= <<<"MENU"
<li class="admin_menu_item">
    <a href="/admin/exit.php">Выход</a>
</li>
</ul>
MENU;
